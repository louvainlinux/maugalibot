from time import time
import aiohttp
import asyncio

from mautrix.types import TextMessageEventContent, MessageType, Format, RelatesTo, RelationType, MediaMessageEventContent
from maubot import Plugin, MessageEvent
from maubot.handlers import command


class MaugaliBot(Plugin):
  # Highly inspired by https://github.com/maubot/echo
  @command.new("magaping", help="MagaPing")
  async def ping_handler(self, evt: MessageEvent, message: str = "") -> None:
    diff = int(time() * 1000) - evt.timestamp
    
    content = TextMessageEventContent(
      msgtype=MessageType.NOTICE, format=Format.HTML,
      body=f"{evt.sender}: MagaPong! (ping took {diff} ms to arrive)",
      formatted_body=f"<a href='https://matrix.to/#/{evt.sender}'>{evt.sender}</a>: MagaPong! "
      f"(<a href='https://matrix.to/#/{evt.room_id}/{evt.event_id}'>ping</a> "
      f"took {diff} ms to arrive)",
      relates_to=RelatesTo(
          rel_type=RelationType("xyz.maubot.pong"),
          event_id=evt.event_id,
      ))

    await evt.respond(content)
